exports.index = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	let category = "",
		renderTemplate = "index";

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		let dbName = db.db('shop');
		var collection = dbName.collection('categories');

		collection.find().toArray(function(err, items) {

			if (req.query.id === "mens") {
				category = _.where(items, {id: 'mens'});
				renderTemplate = "mens";
			} else {
				category = _.where(items, {id: 'womens'});
			}

			res.render(renderTemplate, {
				// Underscore.js lib
				_     : _,

				// Template data
				title: "Simple Store",
				indexLink: "http://localhost/",
				items: items,
				category: category
			});

			db.close();
		});
	});
};


exports.hello = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		let dbName = db.db('shop');
		var collection = dbName.collection('categories');
		
		collection.find().toArray(function(err, items) {

			res.render("hello", {
				// Underscore.js lib
				_     : _,
				
				// Template data
				title : "Hello World!",
				items : items
			});

			db.close();
		});
	});
};